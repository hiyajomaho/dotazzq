<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:74:"D:\dev\dotaFastadmin\public/../application/admin\view\occupation\edit.html";i:1548898614;s:63:"D:\dev\dotaFastadmin\application\admin\view\layout\default.html";i:1548827684;s:60:"D:\dev\dotaFastadmin\application\admin\view\common\meta.html";i:1548827683;s:62:"D:\dev\dotaFastadmin\application\admin\view\common\script.html";i:1548827683;}*/ ?>
<!DOCTYPE html>
<html lang="<?php echo $config['language']; ?>">
    <head>
        <meta charset="utf-8">
<title><?php echo (isset($title) && ($title !== '')?$title:''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">

<link rel="shortcut icon" href="/assets/img/favicon.ico" />
<!-- Loading Bootstrap -->
<link href="/assets/css/backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.css?v=<?php echo \think\Config::get('site.version'); ?>" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
    var require = {
        config:  <?php echo json_encode($config); ?>
    };
</script>
    </head>

    <body class="inside-header inside-aside <?php echo defined('IS_DIALOG') && IS_DIALOG ? 'is-dialog' : ''; ?>">
        <div id="main" role="main">
            <div class="tab-content tab-addtabs">
                <div id="content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <section class="content-header hide">
                                <h1>
                                    <?php echo __('Dashboard'); ?>
                                    <small><?php echo __('Control panel'); ?></small>
                                </h1>
                            </section>
                            <?php if(!IS_DIALOG && !$config['fastadmin']['multiplenav']): ?>
                            <!-- RIBBON -->
                            <div id="ribbon">
                                <ol class="breadcrumb pull-left">
                                    <li><a href="dashboard" class="addtabsit"><i class="fa fa-dashboard"></i> <?php echo __('Dashboard'); ?></a></li>
                                </ol>
                                <ol class="breadcrumb pull-right">
                                    <?php foreach($breadcrumb as $vo): ?>
                                    <li><a href="javascript:;" data-url="<?php echo $vo['url']; ?>"><?php echo $vo['title']; ?></a></li>
                                    <?php endforeach; ?>
                                </ol>
                            </div>
                            <!-- END RIBBON -->
                            <?php endif; ?>
                            <div class="content">
                                <form id="edit-form" class="form-horizontal" role="form" data-toggle="validator" method="POST" action="">

    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('O_name'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-o_name" data-rule="required" class="form-control" name="row[o_name]" type="text" value="<?php echo $row['o_name']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('O_keyword'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-o_keyword" data-rule="required" class="form-control" name="row[o_keyword]" type="text" value="<?php echo $row['o_keyword']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('O_desc1'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-o_desc1" class="form-control" name="row[o_desc1]" type="text" value="<?php echo $row['o_desc1']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('O_desc2'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-o_desc2" data-rule="required" class="form-control" name="row[o_desc2]" type="text" value="<?php echo $row['o_desc2']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('O_desc3'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-o_desc3" data-rule="required" class="form-control" name="row[o_desc3]" type="text" value="<?php echo $row['o_desc3']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('O_characteristic'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <textarea id="c-o_characteristic" class="form-control " rows="5" name="row[o_characteristic]" cols="50"><?php echo $row['o_characteristic']; ?></textarea>
        </div>
    </div>
    <div class="form-group layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled"><?php echo __('OK'); ?></button>
            <button type="reset" class="btn btn-default btn-embossed"><?php echo __('Reset'); ?></button>
        </div>
    </div>
</form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/assets/js/require<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js" data-main="/assets/js/require-backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js?v=<?php echo $site['version']; ?>"></script>
    </body>
</html>