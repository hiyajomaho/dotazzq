define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'hero/index',
                    add_url: 'hero/add',
                    edit_url: 'hero/edit',
                    del_url: 'hero/del',
                    multi_url: 'hero/multi',
                    table: 'hero',
                }
            });
            $("#occinfoss").html(occupation[1].o_characteristic);
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',

                columns: [
                    [
                        // {checkbox: true},
                        {field: 'id', title: __('Id'),sortable: true},
                        {field: 'sprice', title: __('Sprice'),sortable: true},
                        {field: 'name', title: __('Name')},
                        {field: 'nickname', title: __('Nickname')},
                        {field: 'skill_1', title: __('Skill_1'),searchable:false},
                        {field: 'skill_2', title: __('Skill_2'),searchable:false},
                        {field: 'skill_3', title: __('Skill_3'),searchable:false},
                        {field: 'o_id', title: __('O_id'),sortable: true,searchList: occ_id2name,formatter: Controller.api.occ_id2name},
                        {field: 'r_id', title: __('R_id'),sortable: true,searchList: race_id2name,formatter: Controller.api.race_id2name},
                        {field: 'r_id2', title: __('R_id2'),searchable:false,formatter: Controller.api.race_id2name},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],
                queryParams:function(params){
                    Controller.api.showRaceAndOccInfo(params);
                    return params;
                },
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {

            // console.log(occupation);
            // $("#c-o_id").selectPage({
            //     data : occupation,
            //     showField:'o_name'
            // });

            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            occ_id2name:function(value, row, index){

                return occ_id2name[value] ? occ_id2name[value] : value;
            },
            race_id2name:function(value, row, index){
                return race_id2name[value] ? race_id2name[value] : value;
            },
            showRaceAndOccInfo:function(strinfo){
                
                if(strinfo.filter == "{}") return;
                let filter =  JSON.parse(strinfo.filter);
                
                if(filter.r_id != null && filter.r_id != undefined){
                    $("#raceinfo").empty();
                    let html = "";
                    html+= "<dt>种族：</dt>";
                    html+= "<dd>"+race[filter.r_id].r_name+"</dd>";
                    html+= "<dt>关键词：</dt>";
                    html+= "<dd>"+race[filter.r_id].r_keyword+"</dd>";
                    html+= "<dt>特性一：</dt>";
                    html+= "<dd>"+race[filter.r_id].r_desc1+"</dd>";
                    html+= "<dt>特性二：</dt>";
                    html+= "<dd>"+race[filter.r_id].r_desc2+"</dd>";
                    html+= "<dt>特性三：</dt>";
                    html+= "<dd>"+race[filter.r_id].r_desc3+"</dd>";
                    html+= "<dt>推荐：</dt>";
                    html+= "<dd>"+race[filter.r_id].r_characteristic+"</dd>";
                    $("#raceinfo").append(html);
                }

                if(filter.o_id != null && filter.o_id != undefined){
                     $("#occinfo").empty();
                    let html = "";
                    html+= "<dt>种族：</dt>";
                    html+= "<dd>"+occupation[filter.o_id].o_name+"</dd>";
                    html+= "<dt>关键词：</dt>";
                    html+= "<dd>"+occupation[filter.o_id].o_keyword+"</dd>";
                    html+= "<dt>特性一：</dt>";
                    html+= "<dd>"+occupation[filter.o_id].o_desc1+"</dd>";
                    html+= "<dt>特性二：</dt>";
                    html+= "<dd>"+occupation[filter.o_id].o_desc2+"</dd>";
                    html+= "<dt>特性三：</dt>";
                    html+= "<dd>"+occupation[filter.o_id].o_desc3+"</dd>";
                    html+= "<dt>推荐：</dt>";
                    html+= "<dd>"+occupation[filter.o_id].o_characteristic+"</dd>";
                    $("#occinfo").append(html);
                }
                
            },

        }
    };
    return Controller;
});