define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'occupation/index',
                    add_url: 'occupation/add',
                    edit_url: 'occupation/edit',
                    del_url: 'occupation/del',
                    multi_url: 'occupation/multi',
                    table: 'occupation',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'o_name', title: __('O_name')},
                        {field: 'o_keyword', title: __('O_keyword')},
                        {field: 'o_desc1', title: __('O_desc1')},
                        {field: 'o_desc2', title: __('O_desc2')},
                        {field: 'o_desc3', title: __('O_desc3')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});