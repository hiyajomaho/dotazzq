-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.5.53 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win32
-- HeidiSQL 版本:                  9.5.0.5268
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 dota.dotaadmin 结构
CREATE TABLE IF NOT EXISTS `dotaadmin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) NOT NULL DEFAULT '' COMMENT '密码盐',
  `avatar` varchar(100) NOT NULL DEFAULT '' COMMENT '头像',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `logintime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录时间',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `token` varchar(59) NOT NULL DEFAULT '' COMMENT 'Session标识',
  `status` varchar(30) NOT NULL DEFAULT 'normal' COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='管理员表';

-- 正在导出表  dota.dotaadmin 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `dotaadmin` DISABLE KEYS */;
INSERT INTO `dotaadmin` (`id`, `username`, `nickname`, `password`, `salt`, `avatar`, `email`, `loginfailure`, `logintime`, `createtime`, `updatetime`, `token`, `status`) VALUES
	(1, 'admin', 'Admin', '8f04217bafb48bfe1aaf15065175f1d1', 'c38e64', '/assets/img/avatar.png', 'admin@admin.com', 0, 1548827981, 1492186163, 1548838922, '', 'normal');
/*!40000 ALTER TABLE `dotaadmin` ENABLE KEYS */;

-- 导出  表 dota.dotaadmin_log 结构
CREATE TABLE IF NOT EXISTS `dotaadmin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `username` varchar(30) NOT NULL DEFAULT '' COMMENT '管理员名字',
  `url` varchar(1500) NOT NULL DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '日志标题',
  `content` text NOT NULL COMMENT '内容',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  PRIMARY KEY (`id`),
  KEY `name` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='管理员日志表';

-- 正在导出表  dota.dotaadmin_log 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `dotaadmin_log` DISABLE KEYS */;
INSERT INTO `dotaadmin_log` (`id`, `admin_id`, `username`, `url`, `title`, `content`, `ip`, `useragent`, `createtime`) VALUES
	(1, 0, 'Unknown', '/admin/index/login?url=%2Fadmin', '', '{"url":"\\/admin","__token__":"33ccdaa3616870a4f5ecadc545937d2c","username":"admin","captcha":"eddm"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1548827915),
	(2, 0, 'Unknown', '/admin/index/login?url=%2Fadmin', '', '{"url":"\\/admin","__token__":"569aa9ec0ff5bf1ee1c2779d1600b95f","username":"admin","captcha":"ANMN"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1548827925),
	(3, 1, 'admin', '/admin/index/login?url=%2Fadmin', '登录', '{"url":"\\/admin","__token__":"e2c09de88a7dd9da121e28340f2331b4","username":"admin","captcha":"ANMN"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1548827982),
	(4, 1, 'admin', '/admin/hero/edit/ids/35?dialog=1', '英雄管理 编辑', '{"dialog":"1","row":{"sprice":"5","name":"\\u77ee\\u4eba\\u76f4\\u5347\\u673a","nickname":"\\u98de\\u673a","skill_1":"","skill_2":"","skill_3":"","o_id":"6","r_id":"12","r_id2":""},"ids":"35"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1548832947),
	(5, 1, 'admin', '/admin/hero/edit/ids/35?dialog=1', '英雄管理 编辑', '{"dialog":"1","row":{"sprice":"5","name":"\\u77ee\\u4eba\\u76f4\\u5347\\u673a","nickname":"\\u98de\\u673a","skill_1":"","skill_2":"","skill_3":"","o_id":"6","r_id":"12","r_id2":""},"ids":"35"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1548832957);
/*!40000 ALTER TABLE `dotaadmin_log` ENABLE KEYS */;

-- 导出  表 dota.dotaattachment 结构
CREATE TABLE IF NOT EXISTS `dotaattachment` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '物理路径',
  `imagewidth` varchar(30) NOT NULL DEFAULT '' COMMENT '宽度',
  `imageheight` varchar(30) NOT NULL DEFAULT '' COMMENT '高度',
  `imagetype` varchar(30) NOT NULL DEFAULT '' COMMENT '图片类型',
  `imageframes` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片帧数',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `mimetype` varchar(100) NOT NULL DEFAULT '' COMMENT 'mime类型',
  `extparam` varchar(255) NOT NULL DEFAULT '' COMMENT '透传数据',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建日期',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `uploadtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `storage` varchar(100) NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `sha1` varchar(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='附件表';

-- 正在导出表  dota.dotaattachment 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `dotaattachment` DISABLE KEYS */;
INSERT INTO `dotaattachment` (`id`, `admin_id`, `user_id`, `url`, `imagewidth`, `imageheight`, `imagetype`, `imageframes`, `filesize`, `mimetype`, `extparam`, `createtime`, `updatetime`, `uploadtime`, `storage`, `sha1`) VALUES
	(1, 1, 0, '/assets/img/qrcode.png', '150', '150', 'png', 0, 21859, 'image/png', '', 1499681848, 1499681848, 1499681848, 'local', '17163603d0263e4838b9387ff2cd4877e8b018f6');
/*!40000 ALTER TABLE `dotaattachment` ENABLE KEYS */;

-- 导出  表 dota.dotaauth_group 结构
CREATE TABLE IF NOT EXISTS `dotaauth_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父组别',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '组名',
  `rules` text NOT NULL COMMENT '规则ID',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='分组表';

-- 正在导出表  dota.dotaauth_group 的数据：~5 rows (大约)
/*!40000 ALTER TABLE `dotaauth_group` DISABLE KEYS */;
INSERT INTO `dotaauth_group` (`id`, `pid`, `name`, `rules`, `createtime`, `updatetime`, `status`) VALUES
	(1, 0, 'Admin group', '*', 1490883540, 149088354, 'normal'),
	(2, 1, 'Second group', '13,14,16,15,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,61,62,63,64,65,1,9,10,11,7,6,8,2,4,5', 1490883540, 1505465692, 'normal'),
	(3, 2, 'Third group', '1,4,9,10,11,13,14,15,16,17,40,41,42,43,44,45,46,47,48,49,50,55,56,57,58,59,60,61,62,63,64,65,5', 1490883540, 1502205322, 'normal'),
	(4, 1, 'Second group 2', '1,4,13,14,15,16,17,55,56,57,58,59,60,61,62,63,64,65', 1490883540, 1502205350, 'normal'),
	(5, 2, 'Third group 2', '1,2,6,7,8,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34', 1490883540, 1502205344, 'normal');
/*!40000 ALTER TABLE `dotaauth_group` ENABLE KEYS */;

-- 导出  表 dota.dotaauth_group_access 结构
CREATE TABLE IF NOT EXISTS `dotaauth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '会员ID',
  `group_id` int(10) unsigned NOT NULL COMMENT '级别ID',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='权限分组表';

-- 正在导出表  dota.dotaauth_group_access 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `dotaauth_group_access` DISABLE KEYS */;
INSERT INTO `dotaauth_group_access` (`uid`, `group_id`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `dotaauth_group_access` ENABLE KEYS */;

-- 导出  表 dota.dotaauth_rule 结构
CREATE TABLE IF NOT EXISTS `dotaauth_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('menu','file') NOT NULL DEFAULT 'file' COMMENT 'menu为菜单,file为权限节点',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '规则名称',
  `icon` varchar(50) NOT NULL DEFAULT '' COMMENT '图标',
  `condition` varchar(255) NOT NULL DEFAULT '' COMMENT '条件',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为菜单',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `pid` (`pid`),
  KEY `weigh` (`weigh`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='节点表';

-- 正在导出表  dota.dotaauth_rule 的数据：~84 rows (大约)
/*!40000 ALTER TABLE `dotaauth_rule` DISABLE KEYS */;
INSERT INTO `dotaauth_rule` (`id`, `type`, `pid`, `name`, `title`, `icon`, `condition`, `remark`, `ismenu`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
	(1, 'file', 0, 'dashboard', 'Dashboard', 'fa fa-dashboard', '', 'Dashboard tips', 1, 1497429920, 1497429920, 143, 'normal'),
	(2, 'file', 0, 'general', 'General', 'fa fa-cogs', '', '', 1, 1497429920, 1497430169, 137, 'normal'),
	(3, 'file', 0, 'category', 'Category', 'fa fa-list', '', 'Category tips', 1, 1497429920, 1497429920, 119, 'normal'),
	(4, 'file', 0, 'addon', 'Addon', 'fa fa-rocket', '', 'Addon tips', 1, 1502035509, 1502035509, 0, 'normal'),
	(5, 'file', 0, 'auth', 'Auth', 'fa fa-group', '', '', 1, 1497429920, 1497430092, 99, 'normal'),
	(6, 'file', 2, 'general/config', 'Config', 'fa fa-cog', '', 'Config tips', 1, 1497429920, 1497430683, 60, 'normal'),
	(7, 'file', 2, 'general/attachment', 'Attachment', 'fa fa-file-image-o', '', 'Attachment tips', 1, 1497429920, 1497430699, 53, 'normal'),
	(8, 'file', 2, 'general/profile', 'Profile', 'fa fa-user', '', '', 1, 1497429920, 1497429920, 34, 'normal'),
	(9, 'file', 5, 'auth/admin', 'Admin', 'fa fa-user', '', 'Admin tips', 1, 1497429920, 1497430320, 118, 'normal'),
	(10, 'file', 5, 'auth/adminlog', 'Admin log', 'fa fa-list-alt', '', 'Admin log tips', 1, 1497429920, 1497430307, 113, 'normal'),
	(11, 'file', 5, 'auth/group', 'Group', 'fa fa-group', '', 'Group tips', 1, 1497429920, 1497429920, 109, 'normal'),
	(12, 'file', 5, 'auth/rule', 'Rule', 'fa fa-bars', '', 'Rule tips', 1, 1497429920, 1497430581, 104, 'normal'),
	(13, 'file', 1, 'dashboard/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 136, 'normal'),
	(14, 'file', 1, 'dashboard/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 135, 'normal'),
	(15, 'file', 1, 'dashboard/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 133, 'normal'),
	(16, 'file', 1, 'dashboard/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 134, 'normal'),
	(17, 'file', 1, 'dashboard/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 132, 'normal'),
	(18, 'file', 6, 'general/config/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 52, 'normal'),
	(19, 'file', 6, 'general/config/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 51, 'normal'),
	(20, 'file', 6, 'general/config/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 50, 'normal'),
	(21, 'file', 6, 'general/config/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 49, 'normal'),
	(22, 'file', 6, 'general/config/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 48, 'normal'),
	(23, 'file', 7, 'general/attachment/index', 'View', 'fa fa-circle-o', '', 'Attachment tips', 0, 1497429920, 1497429920, 59, 'normal'),
	(24, 'file', 7, 'general/attachment/select', 'Select attachment', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 58, 'normal'),
	(25, 'file', 7, 'general/attachment/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 57, 'normal'),
	(26, 'file', 7, 'general/attachment/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 56, 'normal'),
	(27, 'file', 7, 'general/attachment/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 55, 'normal'),
	(28, 'file', 7, 'general/attachment/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 54, 'normal'),
	(29, 'file', 8, 'general/profile/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 33, 'normal'),
	(30, 'file', 8, 'general/profile/update', 'Update profile', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 32, 'normal'),
	(31, 'file', 8, 'general/profile/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 31, 'normal'),
	(32, 'file', 8, 'general/profile/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 30, 'normal'),
	(33, 'file', 8, 'general/profile/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 29, 'normal'),
	(34, 'file', 8, 'general/profile/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 28, 'normal'),
	(35, 'file', 3, 'category/index', 'View', 'fa fa-circle-o', '', 'Category tips', 0, 1497429920, 1497429920, 142, 'normal'),
	(36, 'file', 3, 'category/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 141, 'normal'),
	(37, 'file', 3, 'category/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 140, 'normal'),
	(38, 'file', 3, 'category/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 139, 'normal'),
	(39, 'file', 3, 'category/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 138, 'normal'),
	(40, 'file', 9, 'auth/admin/index', 'View', 'fa fa-circle-o', '', 'Admin tips', 0, 1497429920, 1497429920, 117, 'normal'),
	(41, 'file', 9, 'auth/admin/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 116, 'normal'),
	(42, 'file', 9, 'auth/admin/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 115, 'normal'),
	(43, 'file', 9, 'auth/admin/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 114, 'normal'),
	(44, 'file', 10, 'auth/adminlog/index', 'View', 'fa fa-circle-o', '', 'Admin log tips', 0, 1497429920, 1497429920, 112, 'normal'),
	(45, 'file', 10, 'auth/adminlog/detail', 'Detail', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 111, 'normal'),
	(46, 'file', 10, 'auth/adminlog/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 110, 'normal'),
	(47, 'file', 11, 'auth/group/index', 'View', 'fa fa-circle-o', '', 'Group tips', 0, 1497429920, 1497429920, 108, 'normal'),
	(48, 'file', 11, 'auth/group/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 107, 'normal'),
	(49, 'file', 11, 'auth/group/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 106, 'normal'),
	(50, 'file', 11, 'auth/group/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 105, 'normal'),
	(51, 'file', 12, 'auth/rule/index', 'View', 'fa fa-circle-o', '', 'Rule tips', 0, 1497429920, 1497429920, 103, 'normal'),
	(52, 'file', 12, 'auth/rule/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 102, 'normal'),
	(53, 'file', 12, 'auth/rule/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 101, 'normal'),
	(54, 'file', 12, 'auth/rule/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 100, 'normal'),
	(55, 'file', 4, 'addon/index', 'View', 'fa fa-circle-o', '', 'Addon tips', 0, 1502035509, 1502035509, 0, 'normal'),
	(56, 'file', 4, 'addon/add', 'Add', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
	(57, 'file', 4, 'addon/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
	(58, 'file', 4, 'addon/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
	(59, 'file', 4, 'addon/local', 'Local install', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
	(60, 'file', 4, 'addon/state', 'Update state', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
	(61, 'file', 4, 'addon/install', 'Install', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
	(62, 'file', 4, 'addon/uninstall', 'Uninstall', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
	(63, 'file', 4, 'addon/config', 'Setting', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
	(64, 'file', 4, 'addon/refresh', 'Refresh', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
	(65, 'file', 4, 'addon/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
	(66, 'file', 0, 'user', 'User', 'fa fa-list', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
	(67, 'file', 66, 'user/user', 'User', 'fa fa-user', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
	(68, 'file', 67, 'user/user/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(69, 'file', 67, 'user/user/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(70, 'file', 67, 'user/user/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(71, 'file', 67, 'user/user/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(72, 'file', 67, 'user/user/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(73, 'file', 66, 'user/group', 'User group', 'fa fa-users', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
	(74, 'file', 73, 'user/group/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(75, 'file', 73, 'user/group/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(76, 'file', 73, 'user/group/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(77, 'file', 73, 'user/group/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(78, 'file', 73, 'user/group/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(79, 'file', 66, 'user/rule', 'User rule', 'fa fa-circle-o', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
	(80, 'file', 79, 'user/rule/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(81, 'file', 79, 'user/rule/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(82, 'file', 79, 'user/rule/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(83, 'file', 79, 'user/rule/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(84, 'file', 79, 'user/rule/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
	(85, 'file', 0, 'hero', '英雄管理', 'fa fa-circle-o', '', '', 1, 1548830546, 1548830546, 0, 'normal'),
	(86, 'file', 85, 'hero/index', '查看', 'fa fa-circle-o', '', '', 0, 1548830546, 1548830546, 0, 'normal'),
	(87, 'file', 85, 'hero/add', '添加', 'fa fa-circle-o', '', '', 0, 1548830546, 1548830546, 0, 'normal'),
	(88, 'file', 85, 'hero/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1548830546, 1548830546, 0, 'normal'),
	(89, 'file', 85, 'hero/del', '删除', 'fa fa-circle-o', '', '', 0, 1548830546, 1548830546, 0, 'normal'),
	(90, 'file', 85, 'hero/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1548830546, 1548830546, 0, 'normal');
/*!40000 ALTER TABLE `dotaauth_rule` ENABLE KEYS */;

-- 导出  表 dota.dotacategory 结构
CREATE TABLE IF NOT EXISTS `dotacategory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '栏目类型',
  `name` varchar(30) NOT NULL DEFAULT '',
  `nickname` varchar(50) NOT NULL DEFAULT '',
  `flag` set('hot','index','recommend') NOT NULL DEFAULT '',
  `image` varchar(100) NOT NULL DEFAULT '' COMMENT '图片',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `diyname` varchar(30) NOT NULL DEFAULT '' COMMENT '自定义名称',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `weigh` (`weigh`,`id`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='分类表';

-- 正在导出表  dota.dotacategory 的数据：~13 rows (大约)
/*!40000 ALTER TABLE `dotacategory` DISABLE KEYS */;
INSERT INTO `dotacategory` (`id`, `pid`, `type`, `name`, `nickname`, `flag`, `image`, `keywords`, `description`, `diyname`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
	(1, 0, 'page', '官方新闻', 'news', 'recommend', '/assets/img/qrcode.png', '', '', 'news', 1495262190, 1495262190, 1, 'normal'),
	(2, 0, 'page', '移动应用', 'mobileapp', 'hot', '/assets/img/qrcode.png', '', '', 'mobileapp', 1495262244, 1495262244, 2, 'normal'),
	(3, 2, 'page', '微信公众号', 'wechatpublic', 'index', '/assets/img/qrcode.png', '', '', 'wechatpublic', 1495262288, 1495262288, 3, 'normal'),
	(4, 2, 'page', 'Android开发', 'android', 'recommend', '/assets/img/qrcode.png', '', '', 'android', 1495262317, 1495262317, 4, 'normal'),
	(5, 0, 'page', '软件产品', 'software', 'recommend', '/assets/img/qrcode.png', '', '', 'software', 1495262336, 1499681850, 5, 'normal'),
	(6, 5, 'page', '网站建站', 'website', 'recommend', '/assets/img/qrcode.png', '', '', 'website', 1495262357, 1495262357, 6, 'normal'),
	(7, 5, 'page', '企业管理软件', 'company', 'index', '/assets/img/qrcode.png', '', '', 'company', 1495262391, 1495262391, 7, 'normal'),
	(8, 6, 'page', 'PC端', 'website-pc', 'recommend', '/assets/img/qrcode.png', '', '', 'website-pc', 1495262424, 1495262424, 8, 'normal'),
	(9, 6, 'page', '移动端', 'website-mobile', 'recommend', '/assets/img/qrcode.png', '', '', 'website-mobile', 1495262456, 1495262456, 9, 'normal'),
	(10, 7, 'page', 'CRM系统 ', 'company-crm', 'recommend', '/assets/img/qrcode.png', '', '', 'company-crm', 1495262487, 1495262487, 10, 'normal'),
	(11, 7, 'page', 'SASS平台软件', 'company-sass', 'recommend', '/assets/img/qrcode.png', '', '', 'company-sass', 1495262515, 1495262515, 11, 'normal'),
	(12, 0, 'test', '测试1', 'test1', 'recommend', '/assets/img/qrcode.png', '', '', 'test1', 1497015727, 1497015727, 12, 'normal'),
	(13, 0, 'test', '测试2', 'test2', 'recommend', '/assets/img/qrcode.png', '', '', 'test2', 1497015738, 1497015738, 13, 'normal');
/*!40000 ALTER TABLE `dotacategory` ENABLE KEYS */;

-- 导出  表 dota.dotaconfig 结构
CREATE TABLE IF NOT EXISTS `dotaconfig` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '变量名',
  `group` varchar(30) NOT NULL DEFAULT '' COMMENT '分组',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '变量标题',
  `tip` varchar(100) NOT NULL DEFAULT '' COMMENT '变量描述',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '类型:string,text,int,bool,array,datetime,date,file',
  `value` text NOT NULL COMMENT '变量值',
  `content` text NOT NULL COMMENT '变量字典数据',
  `rule` varchar(100) NOT NULL DEFAULT '' COMMENT '验证规则',
  `extend` varchar(255) NOT NULL DEFAULT '' COMMENT '扩展属性',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统配置';

-- 正在导出表  dota.dotaconfig 的数据：~17 rows (大约)
/*!40000 ALTER TABLE `dotaconfig` DISABLE KEYS */;
INSERT INTO `dotaconfig` (`id`, `name`, `group`, `title`, `tip`, `type`, `value`, `content`, `rule`, `extend`) VALUES
	(1, 'name', 'basic', 'Site name', '请填写站点名称', 'string', 'FastAdmin', '', 'required', ''),
	(2, 'beian', 'basic', 'Beian', '粤ICP备15054802号-4', 'string', '', '', '', ''),
	(3, 'cdnurl', 'basic', 'Cdn url', '如果静态资源使用第三方云储存请配置该值', 'string', '', '', '', ''),
	(4, 'version', 'basic', 'Version', '如果静态资源有变动请重新配置该值', 'string', '1.0.1', '', 'required', ''),
	(5, 'timezone', 'basic', 'Timezone', '', 'string', 'Asia/Shanghai', '', 'required', ''),
	(6, 'forbiddenip', 'basic', 'Forbidden ip', '一行一条记录', 'text', '', '', '', ''),
	(7, 'languages', 'basic', 'Languages', '', 'array', '{"backend":"zh-cn","frontend":"zh-cn"}', '', 'required', ''),
	(8, 'fixedpage', 'basic', 'Fixed page', '请尽量输入左侧菜单栏存在的链接', 'string', 'dashboard', '', 'required', ''),
	(9, 'categorytype', 'dictionary', 'Category type', '', 'array', '{"default":"Default","page":"Page","article":"Article","test":"Test"}', '', '', ''),
	(10, 'configgroup', 'dictionary', 'Config group', '', 'array', '{"basic":"Basic","email":"Email","dictionary":"Dictionary","user":"User","example":"Example"}', '', '', ''),
	(11, 'mail_type', 'email', 'Mail type', '选择邮件发送方式', 'select', '1', '["Please select","SMTP","Mail"]', '', ''),
	(12, 'mail_smtp_host', 'email', 'Mail smtp host', '错误的配置发送邮件会导致服务器超时', 'string', 'smtp.qq.com', '', '', ''),
	(13, 'mail_smtp_port', 'email', 'Mail smtp port', '(不加密默认25,SSL默认465,TLS默认587)', 'string', '465', '', '', ''),
	(14, 'mail_smtp_user', 'email', 'Mail smtp user', '（填写完整用户名）', 'string', '10000', '', '', ''),
	(15, 'mail_smtp_pass', 'email', 'Mail smtp password', '（填写您的密码）', 'string', 'password', '', '', ''),
	(16, 'mail_verify_type', 'email', 'Mail vertify type', '（SMTP验证方式[推荐SSL]）', 'select', '2', '["None","TLS","SSL"]', '', ''),
	(17, 'mail_from', 'email', 'Mail from', '', 'string', '10000@qq.com', '', '', '');
/*!40000 ALTER TABLE `dotaconfig` ENABLE KEYS */;

-- 导出  表 dota.dotaems 结构
CREATE TABLE IF NOT EXISTS `dotaems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `event` varchar(30) NOT NULL DEFAULT '' COMMENT '事件',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '邮箱',
  `code` varchar(10) NOT NULL DEFAULT '' COMMENT '验证码',
  `times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '验证次数',
  `ip` varchar(30) NOT NULL DEFAULT '' COMMENT 'IP',
  `createtime` int(10) unsigned DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='邮箱验证码表';

-- 正在导出表  dota.dotaems 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `dotaems` DISABLE KEYS */;
/*!40000 ALTER TABLE `dotaems` ENABLE KEYS */;

-- 导出  表 dota.dotahero 结构
CREATE TABLE IF NOT EXISTS `dotahero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sprice` int(11) NOT NULL DEFAULT '1' COMMENT '价格',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `skill_1` varchar(50) DEFAULT NULL COMMENT '技能1',
  `skill_2` varchar(50) DEFAULT NULL COMMENT '技能2',
  `skill_3` varchar(50) DEFAULT NULL COMMENT '技能3',
  `o_id` int(11) DEFAULT NULL COMMENT '职业',
  `r_id` int(11) DEFAULT NULL COMMENT '种族',
  `r_id2` int(11) DEFAULT NULL COMMENT '第二种族',
  PRIMARY KEY (`id`),
  KEY `o_id` (`o_id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COMMENT='英雄表';

-- 正在导出表  dota.dotahero 的数据：52 rows
/*!40000 ALTER TABLE `dotahero` DISABLE KEYS */;
INSERT INTO `dotahero` (`id`, `sprice`, `name`, `nickname`, `skill_1`, `skill_2`, `skill_3`, `o_id`, `r_id`, `r_id2`) VALUES
	(1, 1, '斧王', '斧王', NULL, NULL, NULL, 1, 2, NULL),
	(2, 1, '巨牙海民', '海民', NULL, NULL, NULL, 1, 3, NULL),
	(3, 2, '剑圣', '剑圣', NULL, NULL, NULL, 1, 2, NULL),
	(4, 2, '鱼人守卫', '鱼人', NULL, NULL, NULL, 1, 10, NULL),
	(5, 3, '狼人', '狼人', NULL, NULL, NULL, 1, 3, 9),
	(6, 4, '末日使者', '末日', NULL, NULL, NULL, 1, 1, NULL),
	(7, 4, '海军上将', '船长', NULL, NULL, NULL, 1, 9, NULL),
	(8, 4, '巨魔战将', '巨魔', NULL, NULL, NULL, 1, 7, NULL),
	(9, 1, '魅惑魔女', '小鹿', NULL, NULL, NULL, 2, 3, NULL),
	(10, 2, '树精卫士', '大树', NULL, NULL, NULL, 2, 8, NULL),
	(11, 2, '先知', '先知', NULL, NULL, NULL, 2, 8, NULL),
	(12, 4, '利爪德鲁伊', '熊德', NULL, NULL, NULL, 2, 3, NULL),
	(13, 1, '食人魔法师', '蓝胖', NULL, NULL, NULL, 3, 4, NULL),
	(14, 2, '水晶室女', '冰女', NULL, NULL, NULL, 3, 9, NULL),
	(15, 2, '精灵龙', '帕克', NULL, NULL, NULL, 3, 8, 13),
	(16, 3, '闪电幽魂', '电魂', NULL, NULL, NULL, 3, 11, NULL),
	(17, 3, '秀逗魔导士', '魔女', NULL, NULL, NULL, 3, 9, NULL),
	(18, 4, '光之守卫', '光法', NULL, NULL, NULL, 3, 9, NULL),
	(19, 5, '巫妖', '巫妖', NULL, NULL, NULL, 3, 5, NULL),
	(20, 1, '卓尔游侠', '小黑', NULL, NULL, NULL, 4, 5, NULL),
	(21, 2, '兽王', '兽王', NULL, NULL, NULL, 4, 2, NULL),
	(22, 3, '风行者', '风行', NULL, NULL, NULL, 4, 8, NULL),
	(23, 4, '蛇发女妖', '美杜莎', NULL, NULL, NULL, 4, 10, NULL),
	(24, 5, '潮汐猎人', '潮汐', NULL, NULL, NULL, 4, 10, NULL),
	(25, 1, '赏金猎人', '赏金', NULL, NULL, NULL, 5, 6, NULL),
	(26, 2, '痛苦女王', '女王', NULL, NULL, NULL, 5, 1, NULL),
	(27, 3, '幻影刺客', '幻刺', NULL, NULL, NULL, 5, 8, NULL),
	(28, 3, '沙王', '沙王', NULL, NULL, NULL, 5, 3, NULL),
	(29, 3, '鱼人夜行者', '小鱼', NULL, NULL, NULL, 5, 10, NULL),
	(30, 3, '冥界亚龙', '亚龙', NULL, NULL, NULL, 5, 13, NULL),
	(31, 4, '圣堂刺客', '圣堂刺客', NULL, NULL, NULL, 5, 8, NULL),
	(32, 1, '发条技师', '发条', NULL, NULL, NULL, 6, 6, NULL),
	(33, 1, '修补匠', 'TA', NULL, NULL, NULL, 6, 6, NULL),
	(34, 2, '伐木机', '伐木机', NULL, NULL, NULL, 6, 6, NULL),
	(35, 5, '矮人直升机', '飞机', '', '', '', 6, 12, 0),
	(36, 5, '地精工程师', '炸弹人', NULL, NULL, NULL, 6, 6, NULL),
	(37, 1, '暗影萨满', '暗影萨满', NULL, NULL, NULL, 7, 7, NULL),
	(38, 4, '干扰者', '萨尔', NULL, NULL, NULL, 7, 2, NULL),
	(39, 1, '蝙蝠骑士', '蝙蝠', NULL, NULL, NULL, 8, 7, NULL),
	(40, 2, '混沌骑士', '混沌', NULL, NULL, NULL, 8, 1, NULL),
	(41, 2, '月之骑士', '月骑', NULL, NULL, NULL, 8, 8, NULL),
	(42, 3, '全能骑士', '全能', NULL, NULL, NULL, 8, 9, NULL),
	(43, 3, '死亡骑士', '死骑', NULL, NULL, NULL, 8, 5, NULL),
	(44, 4, '龙骑士', '龙骑', NULL, NULL, NULL, 8, 9, 13),
	(45, 2, '巫医', '巫医', NULL, NULL, NULL, 9, 6, NULL),
	(46, 3, '剧毒术士', '剧毒', NULL, NULL, NULL, 9, 3, NULL),
	(47, 3, '影魔', '影魔', NULL, NULL, NULL, 9, 1, NULL),
	(48, 4, '死灵法师', 'ac', NULL, NULL, NULL, 9, 5, NULL),
	(49, 4, '炼金术士', '炼金', NULL, NULL, NULL, 9, 6, NULL),
	(50, 5, '谜团', '谜团', NULL, NULL, NULL, 9, 11, NULL),
	(51, 1, '敌法师', '敌法', NULL, NULL, NULL, 10, 8, NULL),
	(53, 3, '狙击手', '火枪', NULL, NULL, NULL, 4, 12, NULL);
/*!40000 ALTER TABLE `dotahero` ENABLE KEYS */;

-- 导出  表 dota.dotaoccupation 结构
CREATE TABLE IF NOT EXISTS `dotaoccupation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `o_name` varchar(100) NOT NULL COMMENT '职业',
  `o_keyword` varchar(50) NOT NULL COMMENT '关键词',
  `o_desc1` varchar(100) DEFAULT NULL COMMENT '特点1',
  `o_desc2` varchar(100) NOT NULL COMMENT '特点2',
  `o_desc3` varchar(100) NOT NULL COMMENT '特点3',
  `o_characteristic` text COMMENT '推荐',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='职业';

-- 正在导出表  dota.dotaoccupation 的数据：10 rows
/*!40000 ALTER TABLE `dotaoccupation` DISABLE KEYS */;
INSERT INTO `dotaoccupation` (`id`, `o_name`, `o_keyword`, `o_desc1`, `o_desc2`, `o_desc3`, `o_characteristic`) VALUES
	(1, '战士', '护甲', '3战士 战士护甲+8', '6战士 战士护甲+10', '', '身板硬 前期易凑 价格平滑 后期缺少伤害'),
	(2, '德鲁伊', '速三星', '任意两种在场 可用两个一星合成二星', '四个在场 可用两个二星合成三星', '', '打工皇帝'),
	(3, '法师', '魔抗', '3法师 所有敌军魔抗-30', '6法师 所有敌军魔抗-60', '', '核心依靠冰女先手打出大招aoe 前期弱 打钱为主 可用冰女，女王度过中期<br>\r\n推荐搭配 <br>\r\n1.龙骑 毒龙 组成6法四人类阵容<br>\r\n2.船长 潮汐 飞机 炸弹人 此类控制aoe英雄<br>'),
	(4, '猎人', '高攻击', '3猎人 猎人增加 25% 攻击', '6猎人 猎人增加 35% 攻击', '', '日常找潮汐 比较脆 自带双娜迦的魔抗buff<br>\r\n推荐<br>\r\n1.配合死骑 大树 或 敌法等肉前排 提供亡灵的减甲buff  和精灵的闪避buff<br>\r\n2.面对法师可以考虑分散站位'),
	(5, '刺客', '暴击&切后排', '3刺客 刺客有10%概率造成4倍伤害', '6刺客 刺客有20%概率造成4倍伤害', '', '跳后排 脆 <br>\r\n推荐：<br>\r\n1.冰女 打出先手技能<br>\r\n2.阵容搭配很重要 '),
	(6, '工匠', '回血', '2工匠 工匠生命回复+10 ', '4工匠 工匠生命回复+20', '', '前期英雄 很难凑齐<br>\r\n推荐<br>\r\n1.炸弹人 和 飞机伤害很高 可以用来搭配任何阵容<br>\r\n2.前期可用两个工匠撑住前期\r\n'),
	(7, '萨满', '控制', '2萨满 战斗开始将一名随机地方棋子变成青蛙6s', '', '', '双萨满金钱差距大<br>\r\n推荐<br>\r\n1.前期拿萨满配巨魔阵容 后期补萨尔提升控制力<br>\r\n'),
	(8, '骑士', '减伤', '2骑士 骑士+25%时间有减伤护盾庇护', '4骑士 骑士+35%时间有减伤护盾庇护', '6骑士 骑士+45%时间有减伤护盾庇护', '伤害靠平a<br>\r\n推荐<br>\r\n1.三星龙骑 和 三星月骑 伤害比较高 搭配巨魔的攻速buff和亡灵的减甲buff'),
	(9, '术士', '群体吸血', '3术士 所有友军+15%攻击吸血', '6术士 所有友军+25%攻击吸血', '', '单人战斗力还行<br>\r\n推荐<br>\r\n1.3术士配合猎人阵容 或 巨魔阵容'),
	(10, '恶魔猎手', '敌恶魔', '此棋子视为地方的一个恶魔', '', '', '让敌方恶魔buff失效 一般搭配精灵阵容');
/*!40000 ALTER TABLE `dotaoccupation` ENABLE KEYS */;

-- 导出  表 dota.dotarace 结构
CREATE TABLE IF NOT EXISTS `dotarace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `r_name` varchar(100) DEFAULT NULL COMMENT '种族',
  `r_keyword` varchar(50) DEFAULT NULL COMMENT '关键词',
  `r_desc1` varchar(100) DEFAULT NULL,
  `r_desc2` varchar(100) DEFAULT NULL,
  `r_desc3` varchar(100) DEFAULT NULL,
  `r_characteristic` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='种族表';

-- 正在导出表  dota.dotarace 的数据：13 rows
/*!40000 ALTER TABLE `dotarace` DISABLE KEYS */;
INSERT INTO `dotarace` (`id`, `r_name`, `r_keyword`, `r_desc1`, `r_desc2`, `r_desc3`, `r_characteristic`) VALUES
	(1, '恶魔', '独', '己方场上只有一个恶魔时 攻击力+50%', NULL, NULL, NULL),
	(2, '兽人', '血量高', '2兽人 兽人最大生命值+250', '4兽人 兽人最大生命值+650', NULL, '战士阵容可以凑个萨尔凑齐四兽人'),
	(3, '野兽', '群体加攻', '2野兽 所有友军攻击力+10%', '2野兽 所有友军攻击力+15%', '2野兽 所有友军攻击力+20%', '中期还行 坦度和输出都一般 主要看配合的'),
	(4, '食人魔', '肉', '+10%最大生命值', NULL, NULL, '前期没啥出可以拿一手蓝胖当肉<br>\r\n法师的必经之路'),
	(5, '亡灵', '减甲', '2亡灵 敌军护甲-8', '4亡灵 敌军护甲-10', NULL, '四亡灵加猎人 配合高物理伤害'),
	(6, '地精', '护甲&回血', '3地精 使一个随机友军的护甲+15 生命回复+10', '6地精 所有友方地精的护甲+15 生命回复+10', NULL, '前期可以拿三个低价地精撑前期 后期换成炼金或者炸弹人<br>\r\n六地精不推荐 难度大且伤害不高<br>\r\n推荐<br>\r\n1.火枪 飞机 娜迦 潮汐'),
	(7, '巨魔', '攻速', '2巨魔 所有巨魔攻速+30', '4巨魔 所有友军攻速+30', NULL, '2巨魔 建议选择 暗影萨满 加 巨魔战将<br>\r\n搭配6战士 和 萨尔 组出 战士 兽人 巨魔 萨满多重buff<br>\r\n4巨魔 自带 1骑士 1术士 可以搭配骑术或术士阵容\r\n'),
	(8, '精灵', '闪避', '2精灵 友方精灵+20%闪避', '4精灵 友方精灵+20%闪避', '6精灵 友方精灵+20%闪避', '很容易在中期凑出6精灵阵容 <br>\r\n核心是 帕克 和 TA 可以搭配毒龙 龙骑组成3龙3刺<br>\r\n中期很强'),
	(9, '人类', '缴械', '2人类 所有人类攻击敌人有20%概率缴械敌人3s', '4人类 所有人类攻击敌人有25%概率缴械敌人3s', '6人类 所有人类攻击敌人有30%概率缴械敌人3s', '一般找战士或法师时很容易凑出三人类阵容'),
	(10, '娜迦', '群体魔抗', '2娜迦 友军+30魔抗', '2娜迦 友军+60魔抗', NULL, '后期有空位可以选两高级娜迦  天抗法师'),
	(11, '元素', '魔抗', '2元素 友军元素魔抗+30', NULL, NULL, '没什么必要'),
	(12, '矮人', '射程远', '攻击距离+300', NULL, NULL, '距离远 可以远程打\r\n单体三星还行\r\n'),
	(13, '龙', '先手大', '3龙 所有友方龙初始魔法100', NULL, NULL, '常见组合有 三龙三法  三龙三刺 或者 三龙六法');
/*!40000 ALTER TABLE `dotarace` ENABLE KEYS */;

-- 导出  表 dota.dotasms 结构
CREATE TABLE IF NOT EXISTS `dotasms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `event` varchar(30) NOT NULL DEFAULT '' COMMENT '事件',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `code` varchar(10) NOT NULL DEFAULT '' COMMENT '验证码',
  `times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '验证次数',
  `ip` varchar(30) NOT NULL DEFAULT '' COMMENT 'IP',
  `createtime` int(10) unsigned DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='短信验证码表';

-- 正在导出表  dota.dotasms 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `dotasms` DISABLE KEYS */;
/*!40000 ALTER TABLE `dotasms` ENABLE KEYS */;

-- 导出  表 dota.dotatest 结构
CREATE TABLE IF NOT EXISTS `dotatest` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类ID(单选)',
  `category_ids` varchar(100) NOT NULL COMMENT '分类ID(多选)',
  `week` enum('monday','tuesday','wednesday') NOT NULL COMMENT '星期(单选):monday=星期一,tuesday=星期二,wednesday=星期三',
  `flag` set('hot','index','recommend') NOT NULL DEFAULT '' COMMENT '标志(多选):hot=热门,index=首页,recommend=推荐',
  `genderdata` enum('male','female') NOT NULL DEFAULT 'male' COMMENT '性别(单选):male=男,female=女',
  `hobbydata` set('music','reading','swimming') NOT NULL COMMENT '爱好(多选):music=音乐,reading=读书,swimming=游泳',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `image` varchar(100) NOT NULL DEFAULT '' COMMENT '图片',
  `images` varchar(1500) NOT NULL DEFAULT '' COMMENT '图片组',
  `attachfile` varchar(100) NOT NULL DEFAULT '' COMMENT '附件',
  `keywords` varchar(100) NOT NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `city` varchar(100) NOT NULL DEFAULT '' COMMENT '省市',
  `price` float(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '价格',
  `views` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击',
  `startdate` date DEFAULT NULL COMMENT '开始日期',
  `activitytime` datetime DEFAULT NULL COMMENT '活动时间(datetime)',
  `year` year(4) DEFAULT NULL COMMENT '年',
  `times` time DEFAULT NULL COMMENT '时间',
  `refreshtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '刷新时间(int)',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `switch` tinyint(1) NOT NULL DEFAULT '0' COMMENT '开关',
  `status` enum('normal','hidden') NOT NULL DEFAULT 'normal' COMMENT '状态',
  `state` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '状态值:0=禁用,1=正常,2=推荐',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='测试表';

-- 正在导出表  dota.dotatest 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `dotatest` DISABLE KEYS */;
INSERT INTO `dotatest` (`id`, `admin_id`, `category_id`, `category_ids`, `week`, `flag`, `genderdata`, `hobbydata`, `title`, `content`, `image`, `images`, `attachfile`, `keywords`, `description`, `city`, `price`, `views`, `startdate`, `activitytime`, `year`, `times`, `refreshtime`, `createtime`, `updatetime`, `weigh`, `switch`, `status`, `state`) VALUES
	(1, 0, 12, '12,13', 'monday', 'hot,index', 'male', 'music,reading', '我是一篇测试文章', '<p>我是测试内容</p>', '/assets/img/avatar.png', '/assets/img/avatar.png,/assets/img/qrcode.png', '/assets/img/avatar.png', '关键字', '描述', '广西壮族自治区/百色市/平果县', 0.00, 0, '2017-07-10', '2017-07-10 18:24:45', '2017', '18:24:45', 1499682285, 1499682526, 1499682526, 0, 1, 'normal', '1');
/*!40000 ALTER TABLE `dotatest` ENABLE KEYS */;

-- 导出  表 dota.dotauser 结构
CREATE TABLE IF NOT EXISTS `dotauser` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '组别ID',
  `username` varchar(32) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) NOT NULL DEFAULT '' COMMENT '密码盐',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `mobile` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `bio` varchar(100) NOT NULL DEFAULT '' COMMENT '格言',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '余额',
  `score` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '积分',
  `successions` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '连续登录天数',
  `maxsuccessions` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '最大连续登录天数',
  `prevtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上次登录时间',
  `logintime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录时间',
  `loginip` varchar(50) NOT NULL DEFAULT '' COMMENT '登录IP',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `joinip` varchar(50) NOT NULL DEFAULT '' COMMENT '加入IP',
  `jointime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '加入时间',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `token` varchar(50) NOT NULL DEFAULT '' COMMENT 'Token',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  `verification` varchar(255) NOT NULL DEFAULT '' COMMENT '验证',
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `email` (`email`),
  KEY `mobile` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='会员表';

-- 正在导出表  dota.dotauser 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `dotauser` DISABLE KEYS */;
INSERT INTO `dotauser` (`id`, `group_id`, `username`, `nickname`, `password`, `salt`, `email`, `mobile`, `avatar`, `level`, `gender`, `birthday`, `bio`, `money`, `score`, `successions`, `maxsuccessions`, `prevtime`, `logintime`, `loginip`, `loginfailure`, `joinip`, `jointime`, `createtime`, `updatetime`, `token`, `status`, `verification`) VALUES
	(1, 1, 'admin', 'admin', 'c13f62012fd6a8fdf06b3452a94430e5', 'rpR6Bv', 'admin@163.com', '13888888888', '/assets/img/avatar.png', 0, 0, '2017-04-15', '', 0.00, 0, 1, 1, 1548827731, 1548827750, '127.0.0.1', 0, '127.0.0.1', 1491461418, 0, 1548827750, '', 'normal', '');
/*!40000 ALTER TABLE `dotauser` ENABLE KEYS */;

-- 导出  表 dota.dotauser_group 结构
CREATE TABLE IF NOT EXISTS `dotauser_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '' COMMENT '组名',
  `rules` text COMMENT '权限节点',
  `createtime` int(10) DEFAULT NULL COMMENT '添加时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `status` enum('normal','hidden') DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='会员组表';

-- 正在导出表  dota.dotauser_group 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `dotauser_group` DISABLE KEYS */;
INSERT INTO `dotauser_group` (`id`, `name`, `rules`, `createtime`, `updatetime`, `status`) VALUES
	(1, '默认组', '1,2,3,4,5,6,7,8,9,10,11,12', 1515386468, 1516168298, 'normal');
/*!40000 ALTER TABLE `dotauser_group` ENABLE KEYS */;

-- 导出  表 dota.dotauser_money_log 结构
CREATE TABLE IF NOT EXISTS `dotauser_money_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更余额',
  `before` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更前余额',
  `after` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更后余额',
  `memo` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='会员余额变动表';

-- 正在导出表  dota.dotauser_money_log 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `dotauser_money_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `dotauser_money_log` ENABLE KEYS */;

-- 导出  表 dota.dotauser_rule 结构
CREATE TABLE IF NOT EXISTS `dotauser_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT NULL COMMENT '父ID',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `title` varchar(50) DEFAULT '' COMMENT '标题',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `ismenu` tinyint(1) DEFAULT NULL COMMENT '是否菜单',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) DEFAULT '0' COMMENT '权重',
  `status` enum('normal','hidden') DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='会员规则表';

-- 正在导出表  dota.dotauser_rule 的数据：~12 rows (大约)
/*!40000 ALTER TABLE `dotauser_rule` DISABLE KEYS */;
INSERT INTO `dotauser_rule` (`id`, `pid`, `name`, `title`, `remark`, `ismenu`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
	(1, 0, 'index', '前台', '', 1, 1516168079, 1516168079, 1, 'normal'),
	(2, 0, 'api', 'API接口', '', 1, 1516168062, 1516168062, 2, 'normal'),
	(3, 1, 'user', '会员模块', '', 1, 1515386221, 1516168103, 12, 'normal'),
	(4, 2, 'user', '会员模块', '', 1, 1515386221, 1516168092, 11, 'normal'),
	(5, 3, 'index/user/login', '登录', '', 0, 1515386247, 1515386247, 5, 'normal'),
	(6, 3, 'index/user/register', '注册', '', 0, 1515386262, 1516015236, 7, 'normal'),
	(7, 3, 'index/user/index', '会员中心', '', 0, 1516015012, 1516015012, 9, 'normal'),
	(8, 3, 'index/user/profile', '个人资料', '', 0, 1516015012, 1516015012, 4, 'normal'),
	(9, 4, 'api/user/login', '登录', '', 0, 1515386247, 1515386247, 6, 'normal'),
	(10, 4, 'api/user/register', '注册', '', 0, 1515386262, 1516015236, 8, 'normal'),
	(11, 4, 'api/user/index', '会员中心', '', 0, 1516015012, 1516015012, 10, 'normal'),
	(12, 4, 'api/user/profile', '个人资料', '', 0, 1516015012, 1516015012, 3, 'normal');
/*!40000 ALTER TABLE `dotauser_rule` ENABLE KEYS */;

-- 导出  表 dota.dotauser_score_log 结构
CREATE TABLE IF NOT EXISTS `dotauser_score_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `score` int(10) NOT NULL DEFAULT '0' COMMENT '变更积分',
  `before` int(10) NOT NULL DEFAULT '0' COMMENT '变更前积分',
  `after` int(10) NOT NULL DEFAULT '0' COMMENT '变更后积分',
  `memo` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='会员积分变动表';

-- 正在导出表  dota.dotauser_score_log 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `dotauser_score_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `dotauser_score_log` ENABLE KEYS */;

-- 导出  表 dota.dotauser_token 结构
CREATE TABLE IF NOT EXISTS `dotauser_token` (
  `token` varchar(50) NOT NULL COMMENT 'Token',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `expiretime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '过期时间',
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='会员Token表';

-- 正在导出表  dota.dotauser_token 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `dotauser_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `dotauser_token` ENABLE KEYS */;

-- 导出  表 dota.dotaversion 结构
CREATE TABLE IF NOT EXISTS `dotaversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `oldversion` varchar(30) NOT NULL DEFAULT '' COMMENT '旧版本号',
  `newversion` varchar(30) NOT NULL DEFAULT '' COMMENT '新版本号',
  `packagesize` varchar(30) NOT NULL DEFAULT '' COMMENT '包大小',
  `content` varchar(500) NOT NULL DEFAULT '' COMMENT '升级内容',
  `downloadurl` varchar(255) NOT NULL DEFAULT '' COMMENT '下载地址',
  `enforce` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '强制更新',
  `createtime` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='版本表';

-- 正在导出表  dota.dotaversion 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `dotaversion` DISABLE KEYS */;
INSERT INTO `dotaversion` (`id`, `oldversion`, `newversion`, `packagesize`, `content`, `downloadurl`, `enforce`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
	(1, '1.1.1,2', '1.2.1', '20M', '更新内容', 'https://www.fastadmin.net/download.html', 1, 1520425318, 0, 0, 'normal');
/*!40000 ALTER TABLE `dotaversion` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
