<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 英雄管理
 *
 * @icon fa fa-circle-o
 */
class Hero extends Backend
{
    
    /**
     * Hero模型对象
     * @var \app\admin\model\Hero
     */
    protected $model = null;
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Hero;

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    public function index(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }

        $race = db("race")->select(); // 种族
        $occupation = db("occupation")->select(); // 职业

        $id2race = [];
        $race_id2name = [];
        foreach ($race as $r) {
            $id2race[$r['id']] = $r;
            $race_id2name[$r['id']] = $r['r_name'];
        }

        $id2occupation = [];
        $occ_id2name = [];
        foreach ($occupation as $o) {
            $id2occupation[$o['id']] = $o;
            $occ_id2name[$o['id']] = $o['o_name'];
        }



        $this->view->assign('race', $id2race);
        $this->view->assign('occupation', $id2occupation);

        $this->view->assign('occ_id2name', $occ_id2name);
        $this->view->assign('race_id2name', $race_id2name);

        return $this->view->fetch();
    }
    public function getoccupation(){
        $occupation = db("occupation")->field("id,o_name name")->select(); // 职业
        $result = array("total" => count($occupation), "rows" => $occupation);
        return json($result);
    }

    public function getrace(){
        $race = db("race")->field("id,r_name name")->select(); // 种族
        $result = array("total" => count($race), "rows" => $race);
        return json($result);
    }

}
